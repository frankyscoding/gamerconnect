import { useEffect, useState } from "react";
import { useParams, useNavigate } from "react-router-dom";
import "./postDetail.css";
import { useAuthContext } from "../Login/auth";

function PostDetails() {
  const [post, setPost] = useState([]);
  const params = useParams();
  const { token } = useAuthContext();
  const navigate = useNavigate();

  useEffect(() => {
    const fetchpost = async () => {
      const postUrl = `${process.env.REACT_APP_POSTS_API_HOST}/getpost/${params.id}`;
      const fetchConfig = {
        method: "GET",
        headers: {
          Authorization: `Bearer ${token}`,
        },
      };
      const response = await fetch(postUrl, fetchConfig);
      console.log(response);
      if (response.ok) {
        const data = await response.json();
        setPost(data);
      }
    };
    if (token === false) {
      navigate("/login");
    }
    fetchpost();
  }, [token, navigate, params.id]);

  const [comment, setComment] = useState("");
  const commentchange = (event) => {
    const value = event.target.value;
    setComment(value);
  };

  const [cur_user, setUser] = useState("");
  const [cur_useranme, setUsername] = useState("");
  useEffect(() => {
    const userData = async () => {
      const url = `${process.env.REACT_APP_POSTS_API_HOST}/token`;
      try {
        const response = await fetch(url, {
          credentials: "include",
        });
        if (response.ok) {
          const data = await response.json();
          console.log(data);
          const user_info = data.account.id;
          const user_username = data.account.username;
          console.log("USERRRRRR INFOOOOOOOOOOO", user_info);
          setUsername(user_username);
          setUser(user_info);
        }
      } catch (e) {}
      return false;
    };
    userData();
  }, [token, navigate, params.id]);

  const handleSubmit = async (e) => {
    e.preventDefault();

    const data = {};
    data.comments = comment;
    data.post_id = post.id;
    data.user_id = cur_user;
    data.username = cur_useranme;

    const postURL = `${process.env.REACT_APP_POSTS_API_HOST}/comment`;
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${token}`,
      },
    };
    const response = await fetch(postURL, fetchConfig);
    if (response.ok) {
      const check = await response.json();
      console.log(check);
      navigate(`/posts/${post.id}`);
      setComment("");
    }
  };

  const [commentslist, setCommentslist] = useState([]);
  useEffect(() => {
    const fetchComments = async () => {
      const url = `${process.env.REACT_APP_POSTS_API_HOST}/getcomments`;
      const fetchConfig = {
        method: "GET",
        headers: {
          Authorization: `Bearer ${token}`,
        },
      };

      const response = await fetch(url, fetchConfig);
      if (response.ok) {
        const data1 = await response.json();
        setCommentslist(data1);
      }
    };
    fetchComments();
  }, [token, comment]);

  return (
    <div id="main-content" className="blog-page">
      <div className="container">
        <div className="row clearfix">
          <div className="col-lg-8 col-md-12 left-box">
            <div className="card3 single_post">
              <div className="body">
                <div className="img-post">
                  <img
                    className="img4"
                    src={`${post.picture_url}`}
                    alt="First slide"
                  />
                </div>
                <h3>
                  <a href="blog-details.html">{post.title}</a>
                </h3>
                <p className="post-text">{post.description}</p>
              </div>
            </div>
            <div className="card3">
              <div className="header">
                <h2>Comments</h2>
              </div>
              <div className="body1">
                {commentslist.map((comments) => {
                  if (comments.post_id === post.id) {
                    return (
                      <div class="card123 p-3">
                        <div class="d-flex justify-content-between align-items-center">
                          <div class="user d-flex flex-row align-items-center">
                            <img
                              src="https://upload.wikimedia.org/wikipedia/commons/thumb/b/b5/Windows_10_Default_Profile_Picture.svg/768px-Windows_10_Default_Profile_Picture.svg.png"
                              width="30"
                              class="user-img rounded-circle mr-2"
                              alt="profile-pic-default"
                            />
                            <span>
                              <small class="font-weight-bold text-primary">
                                {comments.username}
                              </small>{" "}
                              <small class="font-weight-bold">
                                {comments.comments}
                              </small>
                            </span>
                          </div>
                          <small class="datetime-stamp"></small>
                        </div>
                        <div class="action d-flex justify-content-between mt-2 align-items-center">
                          <div class="reply px-4">
                            <small>Remove</small>
                            <span class="dots"></span>
                            <small>Reply</small>
                            <span class="dots"></span>
                            <small>Translate</small>
                          </div>
                          <div class="icons align-items-center">
                            <i class="fa fa-star text-warning"></i>
                            <i class="fa fa-check-circle-o check-icon"></i>
                          </div>
                        </div>
                      </div>
                    );
                  } else {
                    return null;
                  }
                })}
              </div>
            </div>
            <div className="card3">
              <div className="header">
                <h2>Leave a comment </h2>
              </div>
              <div className="body">
                <div className="comment-form">
                  <form className="row clearfix" onSubmit={handleSubmit}>
                    <div className="col-sm-12">
                      <div className="form-group">
                        <textarea
                          rows="4"
                          className="form-control no-resize"
                          placeholder="Comment here"
                          onChange={commentchange}
                          required
                          value={comment}
                        ></textarea>
                      </div>
                      <button
                        type="submit"
                        className="btn btn-block btn-primary"
                      >
                        SUBMIT
                      </button>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
export default PostDetails;
